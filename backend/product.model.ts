import { Document, Schema, model, ObjectId, Model } from "mongoose";
import mongoose from 'mongoose';

export interface Product {
  user: string,
  importalasDatuma: string,
  cimzett: string,
  szallitasiOpcio: string,
  cikkszam: string,
  cikkMegnevezese: string,
  rendelesAzonosito: string,
  ugyfelNeve: string,
  torolt: boolean,
  duplikalt: boolean,
  lezart: boolean
  // TODO: user
}

export interface ProductDocument extends Product, Document { }
export interface ProductModel extends Model<ProductDocument> { }

const ProductSchema: Schema<ProductDocument, ProductModel> = new Schema({
  user: { type: String },
  importalasDatuma: { type: String },
  cimzett: { type: String },
  szallitasiOpcio: { type: String },
  cikkszam: { type: String },
  cikkMegnevezese: { type: String },
  rendelesAzonosito: { type: String },
  ugyfelNeve: { type: String },
  torolt: { type: Boolean },
  duplikalt: { type: Boolean },
  lezart: { type: Boolean }
  // TODO: user

})

export default model('CustomerData', ProductSchema);