

export function loggingMiddleware(req: any, res: any, next: Function) {
	const method = req.method;
	const date = new Date();
	const path = req.path;
	const user = req?.session?.user;
	const logMessage = `Access: ${date} ${method} ${path} ${user}`;

	console.log(logMessage);
	return next();

}