import { Component, Input, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { PaginatonData } from '../pagination/pagination.component';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.scss']
})
export class ProductTableComponent implements OnInit {

  products: Product[] = [];
  product: Product;
  totalItemCount = 0;
  selectedDate = '';
  selectedPage = 1;
  selectedLimit = 50;
  currentNumberOfOrders = 0;
  selectedAdressee = '';
  selectedDeliveryOption = '';
  deliveryOptions: any[];
  selectedArticleNumber = '';
  selectedArticleName = '';

  constructor(
    private productService: ProductService
  ) {

  }

  ngOnInit(): void {
    this.getDeliveryOptions();
    this.getProducts();
  }

  async getProducts(options?: { limit?: number, page?: number, date?: string, adressee?: string, deliveryOption?: string, articleNumber?: string, articleName?: string }) {
    const limit = options?.limit || this.selectedLimit;
    const page = options?.page || this.selectedPage;
    const date = options?.date || this.selectedDate;
    const adressee = options?.adressee || this.selectedAdressee;
    const deliveryOption = options?.deliveryOption || this.selectedDeliveryOption;
    const articleNumber = options?.articleNumber || this.selectedArticleNumber;
    const articleName = options?.articleName || this.selectedArticleName;

    // Ha Observable jön vissza
    this.productService.getProducts(limit, page, date, adressee, deliveryOption, articleNumber, articleName).subscribe(data => {
      this.products = data.items;
      this.totalItemCount = data.count;
      this.selectedLimit = limit;
      this.selectedPage = page;
      this.selectedDate = date;
      this.currentNumberOfOrders = data.currentNumberOfOrders;
      this.selectedAdressee = adressee;
      this.selectedDeliveryOption = deliveryOption;
      this.selectedArticleNumber = articleNumber;
      this.selectedArticleName = articleName;
    });
  }

  toggleProductDeleted(id: string) {
    this.productService.toggleProductDeleted(id).subscribe(response => {
      this.getProducts();
    });
  }

  onSelectedDate() { // without type info
    this.getProducts();
  }
  onSelectedAddressee() {
    this.getProducts();
  }

  getDeliveryOptions() {
    this.productService.getDeliveryOptions().subscribe(response => {
      this.deliveryOptions = response;
    });
  }

  onSelectedArticleNumber() {
    this.getProducts();
  }
  onSelectedDeliveryOption() {
    this.getProducts()
  }
  onSelectedArticleName() {
    this.getProducts();
  }

  onSaved() { // without type info
    this.getProducts();
  }

  onPaginationUpdated($event: PaginatonData) {
    this.getProducts({ limit: $event.limit, page: $event.page });
  }

  onFileImported() {
    this.getProducts();
  }

  getRowBackground(product: Product) {
    if (product.lezart) { return '#D3D3D3' };
    if (product.torolt) { return 'red' };
    if (product.duplikalt) { return 'yellow' };
    return 'white';
  }

}
