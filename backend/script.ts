import parse, { HTMLElement } from 'node-html-parser';
import fs from 'fs';
import { Product } from './product.model';

export interface ColumnHeader {
  cimzett: string | boolean,
  szallitasiOpcio: string | boolean,
  cikkszam: string | boolean,
  cikkMegnevezese: string | boolean,
  rendelesAzonosito: string | boolean,
  ugyfelNeve: string | boolean,
}

export const dataFromXlSX = (inputPath: string, user: string): any => {
  const data = fs.readFileSync(inputPath, 'utf-8');
  const document = parse(data);
  const table = document.childNodes[1].childNodes[3].childNodes[0];
  const rows = table.childNodes;
  const processedRows: Product[] = [];
  const today = new Date();
  const dd = String(today.getDate()).padStart(2, '0');
  const mm = String(today.getMonth() + 1).padStart(2, '0');
  const yyyy = today.getFullYear();
  const importDate = yyyy + '-' + mm + '-' + dd;

  const columnHeader: ColumnHeader = {
    cimzett: rows[1].childNodes[0].childNodes[0].text === 'Címzett' ? rows[1].childNodes[0].childNodes[0].text : false,
    szallitasiOpcio: rows[1].childNodes[0].childNodes[1].text === 'Szállítási opció' ? rows[1].childNodes[0].childNodes[1].text : false,
    cikkszam: rows[1].childNodes[0].childNodes[2].text === 'Cikkszám' ? rows[1].childNodes[0].childNodes[2].text : false,
    cikkMegnevezese: rows[1].childNodes[0].childNodes[3].text === 'Cikk megnevezés' ? rows[1].childNodes[0].childNodes[3].text : false,
    rendelesAzonosito: rows[1].childNodes[0].childNodes[4].text === 'Megrendelés azonosító' ? rows[1].childNodes[0].childNodes[4].text : false,
    ugyfelNeve: rows[1].childNodes[0].childNodes[5].text === 'Ügyfél neve' ? rows[1].childNodes[0].childNodes[5].text : false,
  }
  if(Object.values(columnHeader).includes(false)) {
    return processedRows;
  }

  for (const row of rows) {
    if (row instanceof HTMLElement) {
      if (row.rawTagName !== 'tr') { continue; }
      const processedRow = {
        user: user,
        importalasDatuma: importDate,
        cimzett: row.childNodes[0].text,
        szallitasiOpcio: row.childNodes[1].text,
        cikkszam: row.childNodes[2].text,
        cikkMegnevezese: row.childNodes[3].text,
        rendelesAzonosito: row.childNodes[4].text,
        ugyfelNeve: row.childNodes[5].text,
        torolt: false,
        duplikalt: false,
        lezart: false
      }
      processedRows.push(processedRow);
    }
  }
  return processedRows;
}
