import bcrypt from 'bcrypt';

const password = process.argv[2]
const hash = bcrypt.hashSync(password, 10);
console.log(hash);