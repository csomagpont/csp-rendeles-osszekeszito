import bcrypt from 'bcrypt';

export function initAuthControllers(app: any) {

	const usersWithHashes = [
		{ name: 'admin', hash: '$2b$10$Oe0o4ampgiqW7.WFM/KL6.pV8N/YHcCczox9vFXfxS6ccRsmQ8Lta' },
		{ name: 'szondi', hash: '$2b$10$V3bg73uAf/DESKz/Hi943O72nbl0mDnQ3KVfOOuqRL87k8/YJ9yG6' },
		{ name: 'fehervari', hash: '$2b$10$JUKAjZUTO8XIXN6VxhS2OOoIKwq7qqvuq8BE23SvXwf/14hMDRu7u' },
		{ name: 'dunaharaszti', hash: '$2b$10$ZM0YLQQpBpziGWPjwJQwcenMoXJbT/tZAZuSixX25dCx8C0fQ3ZbS' },
	]

	app.get('/login', (req: any, res: any) => {
		return res.sendFile('view/login.html', { root: '.' });
	})

	app.post('/login', (req: any, res: any) => {
		const username = req.body.user as string;
		const password = req.body.password;

		const userWithHash = usersWithHashes.find(user => username === user.name);
		const hash = userWithHash?.hash;
		if (!hash || !password) {
			return res.sendFile('view/error.html', { root: '.' });
		}
		const success = bcrypt.compareSync(password, hash);
		if (success) {
			req.session.user = username;
			return res.redirect('/index');
		} else {
			return res.sendFile('view/error.html', { root: '.' });
		}
	})

	app.get('/logout', (req: any, res: any) => {
		req.session.user = null;
		return res.redirect('/login');
	})
}