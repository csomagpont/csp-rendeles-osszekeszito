import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent implements OnInit {

  file: File | null;
  chosen: boolean;
  message: string;

  @Output() imported: EventEmitter<void> = new EventEmitter();

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit(): void {
  }

  fileChosen($event: any) {
    if ($event.target.value) {
      this.file = <File>$event.target.files[0];
      this.chosen = true;
      this.message = '';
    }
  }

  submitFile() {
    const formData = new FormData();
    if (this.file) {
      formData.append('file', this.file, this.file.name);
    }
    this.message = 'Feltöltés folyamatban...';
    this.http.post<{message: string}>('/upload', formData).subscribe((response) => {
      if(response.message === 'error' ) {
        alert('Nem sikeres feltöltés, nem jó excel formátumot használt. Kérjük próbálkozzon  újból!');
        this.message = 'Nem sikeres feltöltés, nem jó excel formátumot használt. Kérjük próbálkozzon  újból!';
      } else {
        this.file = null;
        this.chosen = false;
        this.imported.emit();
        this.message = 'Sikeres feltöltés';
      }      
    })
  }
}
