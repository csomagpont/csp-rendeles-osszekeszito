import morgan from 'morgan';

export function authMiddleware(req: any, res: any, next: Function) {
	// Dev
	// req.session.user = 'admin';
	if (req.session.user) {
		return next();
	} else {
		return res.redirect('/login');
	}
}