import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import FileSaver from 'file-saver';

@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.css']
})
export class ExportComponent implements OnInit {

  exportedDate: Date | string | null;
  name: string;

  constructor(private http: HttpClient, private datePipe: DatePipe) { }

  ngOnInit(): void {
  }


  exportFile(query: boolean) {
    let urlQuery = '';
    this.exportedDate = this.generateExportdate();
    if (query === true) {
      urlQuery = `?teteles=${query}`;
      this.name = 'tételes';
    }
    else { this.name = 'összevont' }
    this.http.get(`/export${urlQuery}`, { responseType: "blob" }).subscribe(response => {
      FileSaver.saveAs(response, `exportalt-fajl-${this.name}-${this.exportedDate}.xlsx`)
    })
  }
  generateExportdate() {
    let exportedDate: Date | string | null = new Date();
    return this.datePipe.transform(exportedDate, 'yyyy-MM-dd_HH-mm');
  }

}
