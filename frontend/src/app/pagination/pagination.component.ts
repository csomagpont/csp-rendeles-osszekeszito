import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';


export interface PaginatonData {
  page: number,
  limit: number
}


@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnChanges {

  @Input() totalItemCount = 0;
  @Output() updated: EventEmitter<PaginatonData> = new EventEmitter();

  page = 1;
  totalPages = 0;
  limit = 50;
  limitValues = [50, 250, 500, 1000, 2000];

  constructor() { }

  ngOnChanges() {
    this.calculateTotalPages();
  }

  onLimitChange() {
    this.page = 1;
    this.calculateTotalPages();
    this.update();
  }

  onNextClick() {
    if (this.page === this.totalPages) { return; }
    this.page++;
    this.update();
  }

  onPrevClick() {
    if (this.page === 1) { return; }
    this.page--;
    this.update();
  }

  calculateTotalPages() {
    this.totalPages = Math.ceil(this.totalItemCount / this.limit) || 1;
    if (this.page > this.totalPages) {
      this.page = this.totalPages;
    }
  }

  update() {
    this.updated.emit({
      page: this.page,
      limit: this.limit
    });
  }

}
