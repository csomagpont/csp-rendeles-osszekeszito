import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-save',
  templateUrl: './save.component.html',
  styleUrls: ['./save.component.css']
})
export class SaveComponent implements OnInit {

  @Output() saved: EventEmitter<void> = new EventEmitter();

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  onSaveClick() {
    this.http.post('/save', null).subscribe(response => {
      this.saved.emit();
    });
  }

}
