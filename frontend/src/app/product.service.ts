import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Product } from './product';
import { HttpClient } from '@angular/common/http'
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }
  private url: string = 'data-from-mongodb';


  getProducts(limit: number, page: number, date: string, adressee: string, deliveryOption: string, articleNumber: string, articleName: string): Observable<any> {
    const offset = limit * (page - 1);
    const url = this.url + `?limit=${limit}&page=${page}&date=${date}&adressee=${adressee}&deliveryOption=${deliveryOption}&articleNumber=${articleNumber}&articleName=${articleName}`;
    return this.http.get<{ data: { items: any, count: number, currentNumberOfOrders: number }, success: number }>(url).pipe(
      map(response => response.data),
      map(data => this.markElsoRendelesAzonosito(data)),
      catchError(error => this.handleError<{ limit: number, page: number }>(error))
    );
  }

  toggleProductDeleted(id: string) {
    const url = this.url + '/' + `${id}`;
    return this.http.delete<{ id: string }>(url).pipe(
      catchError(error => this.handleError<{ item: number }>(error))
    );
  }
  getDeliveryOptions() {
    const url = '/get-delivery-options';;
    return this.http.get<{ data: string[] }>(url).pipe(
      map(response => response.data),
      catchError(error => this.handleError<{ item: number }>(error))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: felhasználónak jelezzen vissza
      console.error(error); // log to console instead
      return of(result as T);
    };
  }

  private markElsoRendelesAzonosito(data: { items: any, count: number, currentNumberOfOrders: number }) {
    const items = data.items;
    let lastRendelesAzonosito: string | null = null;
    for (const item of items) {
      if (lastRendelesAzonosito !== item.rendelesAzonosito) {
        item.elsoRendelesAzonosito = true;
      }
      lastRendelesAzonosito = item.rendelesAzonosito;
    }
    return data;
  }

}
