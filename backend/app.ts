import express from 'express';
import mongoose, { isValidObjectId, ObjectId, Schema } from 'mongoose';
import dotenv from 'dotenv';
import session from 'express-session';
import { initControllers } from './app.controller';
import { initAuthControllers } from './auth.controller';
import { authMiddleware } from './auth.middleware';
import { loggingMiddleware } from './logging.middleware';


export const startApp = async (): Promise<void> => {

	dotenv.config({ path: '../.env' });
	const mongoDB: string = process.env.DATABASE_URL as string;
	console.log('mongo string:', mongoDB);
	const app = express();
	const port: number = parseInt(process.env.PORT || '3000');

	mongoose.connect(mongoDB, { useNewUrlParser: true })
		.then(() => {
			console.log("Connected to database");
		})
		.catch(error => {
			console.error("Connection Failed", error);
		});

	app.use(session({
		secret: 'aSG9öasig9aö',
		resave: false,
		saveUninitialized: true
	}));

	app.use(express.urlencoded({ extended: false }));
	app.use(express.json());
	app.use(loggingMiddleware);

	app.use((req, res, next) => {
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Access-Control-Allow-Headers',
			'Origin, X-Requested-With, Content-Type, Accept')
		res.setHeader('Access-Control-Allow-Methods',
			'GET, POST, PATCH, DELETE, OPTIONS')
		next();
	});

	initAuthControllers(app);
	initControllers(app);

	app.use(authMiddleware, express.static('view/angular'))
	app.get('/index', authMiddleware, (req, res) => {
		return res.sendFile('view/angular/index.html', { root: '.' });
	})

	app.use('**', (req, res) => {
		return res.redirect('/login')
	})

	app.listen(port, () => {
		console.log(`App listening at http://localhost:${port}`);
	});
}
