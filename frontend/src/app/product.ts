export interface Product {
  _id: string,
  importalasDatuma: string,
  cimzett: string;
  szallitasiOpcio: string;
  cikkszam: string;
  cikkMegnevezese: string;
  rendelesAzonosito: string;
  torolt: boolean;
  duplikalt: boolean;
  lezart: boolean;
  elsoRendelesAzonosito?: boolean;

}
