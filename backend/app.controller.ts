import fs from 'fs';
import Excel from 'exceljs';
import { dataFromXlSX } from './script';
import customerModel, { Product } from './product.model';
import axios from 'axios';
import multer from 'multer';
import { authMiddleware } from './auth.middleware';

export function initControllers(app: any) {

  console.log('initcontrollers');

  const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => { cb(null, './input') }
  });
  const upload = multer({ storage: fileStorage }).single('file');

  app.delete('/data-from-mongodb/:id', authMiddleware, async (req: any, res: any) => {
    try {
      const id = req.params.id;
      const user = req.session.user;
      const item = await customerModel.findOne({ _id: id, user: user });
      item?.set('torolt', !item.get('torolt')).save();

      res.send({
        success: true
      })
    } catch (error) {
      console.error(error);
      res.status(500).send({
        data: 'Unexpected error',
        success: false
      });
    }
  })

  app.post('/save', authMiddleware, async (req: any, res: any) => {
    try {
      const user = req.session.user;
      await customerModel.updateMany({ user: user, lezart: false }, { $set: { lezart: true } });
      res.send({
        success: true
      })
    } catch (error) {
      console.error(error);
      res.status(500).send({
        data: 'Unexpected error',
        success: false
      });
    }
  })

  app.get(`/data-from-mongodb`, authMiddleware, async (req: any, res: any) => {
    try {
      const page = parseInt(req.query.page as string) || 1;
      const limit = parseInt(req.query.limit as string) || 50;
      const date = (req.query.date as string) || undefined;
      const adressee = (req.query.adressee as string) || undefined;
      const deliveryOption = (req.query.deliveryOption as string) || undefined;
      const articleNumber = (req.query.articleNumber as string) || undefined;
      const articleName = (req.query.articleName as string) || undefined;
      const offset = limit * (page - 1);
      const user = req.session.user;
      const count = await customerModel.count({ user: user }).exec();

      const currentDistinctOrders: any[] = await customerModel.distinct('rendelesAzonosito', { user: user, lezart: false, torolt: false });
      const currentNumberOfOrders = currentDistinctOrders.length;

      const filter: any = {
        user: user
      };

      console.log(user);
      if (date) { filter.importalasDatuma = date }
      if (adressee) { filter.cimzett = adressee }
      if (deliveryOption) { filter.szallitasiOpcio = deliveryOption }
      if (articleNumber) { filter.cikkszam = articleNumber }
      if (articleName) { filter.cikkMegnevezese = articleName }
      console.log(filter);
      const items = await customerModel.find(filter).sort({ lezart: false, rendelesAzonosito: 'asc' }).limit(limit).skip(offset).exec();
      res.send({
        data: {
          items,
          currentNumberOfOrders,
          count
        },
        success: true
      });
    } catch (error) {
      console.error(error);
      res.status(500).send({
        data: 'Unexpected error',
        success: false
      });
    }
  });
  app.get(`/get-delivery-options`, authMiddleware, async (req: any, res: any) => {
    try {
      const user = req.session.user;
      const filter: any = {
        user: user
      }
      const deliveryOptions = await customerModel.find(filter).select('szallitasiOpcio').distinct('szallitasiOpcio').exec();
      res.send({

        data: deliveryOptions,
        success: true
      });
    } catch (error) {
      console.error(error);
      res.status(500).send({
        data: 'Unexpected error',
        success: false
      });
    }
  });

  app.post('/upload', authMiddleware, upload, async (req: any, res: any) => {
    const filePath = req.file.path;
    const user = req.session.user;
    const customersData: Product[] = dataFromXlSX(filePath, user);
    let message: string;
    if(!customersData.length) {
      message = 'error';
      
    } else {
      message = 'CustomersData Added to Database Succesfully';
      for (const index in customersData) {
        const customerData = new customerModel({
          user: customersData[index].user,
          importalasDatuma: customersData[index].importalasDatuma,
          cimzett: customersData[index].cimzett,
          szallitasiOpcio: customersData[index].szallitasiOpcio,
          cikkszam: customersData[index].cikkszam,
          cikkMegnevezese: customersData[index].cikkMegnevezese,
          rendelesAzonosito: customersData[index].rendelesAzonosito,
          ugyfelNeve: customersData[index].ugyfelNeve,
          torolt: customersData[index].torolt,
          duplikalt: customersData[index].duplikalt,
          lezart: customersData[index].lezart
        })
        await customerData.save();
      }
      for (let index in customersData) {
  
        if (await customerModel.findOne({ user: user, lezart: true, rendelesAzonosito: customersData[index].rendelesAzonosito })) {
          await customerModel.updateMany({ user: user, lezart: false, rendelesAzonosito: customersData[index].rendelesAzonosito, duplikalt: false }, { $set: { duplikalt: true, torolt: true } });
        }
      }
      fs.unlinkSync(filePath);
    }
    res.status(201).json({
      message: message,
    })
  })

  app.get(`/export`, authMiddleware, async (req: any, res: any) => {
    const teteles = !!req.query.teteles;
    try {
      const user = req.session.user;
      const page = parseInt(req.query.page as string) || 1;
      const limit = parseInt(req.query.limit as string) || 5;
      const offset = limit * (page - 1);
      const items = await customerModel.find({ user: user, lezart: false, torolt: false }).skip(offset).exec();
      const workbook = new Excel.Workbook();
      const worksheet = workbook.addWorksheet("Exportált Adatok");
      if (!teteles) {
        const columns = [
          { header: 'Címzett', key: 'cimzett', width: 15 },
          { header: 'Szálítási opció', key: 'szallitasiOpcio', width: 15 },
          { header: 'Cikkszám', key: 'cikkszam', width: 15 },
          { header: 'Db', key: 'db', width: 4 },
          { header: 'Cikk megnevezése', key: 'cikkMegnevezese', width: 32 },
        ];
        worksheet.columns = columns;
      }
      else {
        const columns = [
          { header: 'Címzett', key: 'cimzett', width: 15 },
          { header: 'Szálítási opció', key: 'szallitasiOpcio', width: 15 },
          { header: 'Cikkszám', key: 'cikkszam', width: 15 },
          { header: 'Cikk megnevezése', key: 'cikkMegnevezese', width: 32 },
        ];
        worksheet.columns = columns;
      }
      worksheet.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('B1').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('C1').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('D1').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('E1').alignment = { vertical: 'middle', horizontal: 'center' };

      //'MPL', 'Fehér', 'Szondi', 'City', 'Fáma', 'GLS'

      const deliveryOptions: any = {
        '5PL csomagpont ': '5PL',
        'MPL 1 időgarancia': 'MPL',
        'MPL 1 munkanapos átfutási idő': 'MPL',
        'MPL 1 munkanapos időgarancia': 'MPL',
        'Személyes Fehérvári ': 'Fehér',
        'Személyes Szondi': 'Szondi',
        'Citycsomag': 'City',
        'nextDayDelivery': 'Fáma',
        '24H': 'GLS'
      }
      const exportItems: any[] = [];
      for (const item of items) {
        const exportItem = exportItems.find(elem => elem.rendelesAzonosito === item.get('rendelesAzonosito') && elem.cikkszam === item.get('cikkszam'));

        if (exportItem && !teteles) {
          exportItem.db++;
        } else {
          item.szallitasiOpcio = deliveryOptions?.[item.szallitasiOpcio] || item.szallitasiOpcio;
          exportItems.push({
            cimzett: item.get('cimzett'),
            szallitasiOpcio: item.get('szallitasiOpcio'),
            cikkszam: item.get('cikkszam'),
            db: 1,
            cikkMegnevezese: item.get('cikkMegnevezese'),
            rendelesAzonosito: item.get('rendelesAzonosito'),
            ugyfelNeve: item.get('ugyfelNeve')
          });
        }
      }
      const newRendelesAzonositoBorderColor: string = '888888';
      const basicBorderColor: string = 'B8B8B8';
      let lastRendelesAzonosito: string | null = null;
      let clientName: string = '';
      for (const exportItem of exportItems) {

        if (exportItem.ugyfelNeve !== clientName) {

          const obj = {
            cimzett: exportItem.ugyfelNeve,
            szallitasiOpcio: '',
            cikkszam: '',
            db: '',
            cikkMegnevezese: '',
            rendelesAzonosito: '',
            ugyfelNeve: ''
          }
          const row = worksheet.addRow(obj);
          clientName = exportItem.ugyfelNeve;
          row.getCell(1).fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: 'D3D3D3' }
          };
          row.getCell(1).alignment = { wrapText: true, vertical: 'middle', horizontal: 'center' };
        }
        const row = worksheet.addRow(exportItem);

        row.getCell(1).alignment = { wrapText: true, vertical: 'middle', horizontal: 'center' };
        row.getCell(1).border = {
          top: { style: 'thin', color: { argb: `${basicBorderColor}` } },
          bottom: { style: 'thin', color: { argb: `${basicBorderColor}` } },
        };

        row.getCell(2).alignment = { wrapText: true, vertical: 'middle', horizontal: 'center' };
        row.getCell(2).border = {
          top: { style: 'thin', color: { argb: `${basicBorderColor}` } },
          bottom: { style: 'thin', color: { argb: `${basicBorderColor}` } },
        };

        row.getCell(3).alignment = { wrapText: true, vertical: 'middle', horizontal: 'center' };
        row.getCell(3).border = {
          top: { style: 'thin', color: { argb: `${basicBorderColor}` } },
          bottom: { style: 'thin', color: { argb: `${basicBorderColor}` } },
        };

        if (!teteles) {
          row.getCell(4).alignment = { vertical: 'middle', horizontal: 'center' };
          row.getCell(4).border = {
            top: { style: 'thin', color: { argb: `${basicBorderColor}` } },
            bottom: { style: 'thin', color: { argb: `${basicBorderColor}` } },
          };
          row.getCell(5).alignment = { wrapText: true, vertical: 'middle', horizontal: 'center' };
          row.getCell(5).border = {
            top: { style: 'thin', color: { argb: `${basicBorderColor}` } },
            bottom: { style: 'thin', color: { argb: `${basicBorderColor}` } },
          };
        }
        else {
          row.getCell(4).alignment = { wrapText: true, vertical: 'middle', horizontal: 'center' };
          row.getCell(4).border = {
            top: { style: 'thin', color: { argb: `${basicBorderColor}` } },
            bottom: { style: 'thin', color: { argb: `${basicBorderColor}` } },
          };
        }

        if (exportItem.rendelesAzonosito !== lastRendelesAzonosito) {

          row.getCell(1).border = {
            top: { style: 'medium', color: { argb: `${newRendelesAzonositoBorderColor}` } },
            bottom: { style: 'thin', color: { argb: `${basicBorderColor}` } },
          };

          row.getCell(2).border = {
            top: { style: 'medium', color: { argb: `${newRendelesAzonositoBorderColor}` } },
            bottom: { style: 'thin', color: { argb: `${basicBorderColor}` } },
          };

          row.getCell(3).border = {
            top: { style: 'medium', color: { argb: `${newRendelesAzonositoBorderColor}` } },
            bottom: { style: 'thin', color: { argb: `${basicBorderColor}` } },
          };
          if (!teteles) {
            row.getCell(4).border = {
              top: { style: 'medium', color: { argb: `${newRendelesAzonositoBorderColor}` } },
              bottom: { style: 'thin', color: { argb: `${basicBorderColor}` } },
            };

            row.getCell(5).border = {
              top: { style: 'medium', color: { argb: `${newRendelesAzonositoBorderColor}` } },
              bottom: { style: 'thin', color: { argb: `${basicBorderColor}` } },
            };
          } else {
            row.getCell(4).border = {
              top: { style: 'medium', color: { argb: `${newRendelesAzonositoBorderColor}` } },
              bottom: { style: 'thin', color: { argb: `${basicBorderColor}` } },
            };
          }
        }
        lastRendelesAzonosito = exportItem.rendelesAzonosito;
      }

      await workbook.xlsx.writeFile('xlsx-files/export.xlsx');
      res.download('xlsx-files/export.xlsx');

    } catch (error) {
      console.error(error);
      res.status(500).send({
        data: 'Unexpected error',
        success: false
      });
    }
  });

}
